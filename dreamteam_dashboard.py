from sklearn.ensemble import RandomForestClassifier
import xgboost as xgb
import pandas as pd
import numpy as np
from datetime import timedelta

from bokeh.models import ColumnDataSource, Plot, LinearAxis, Grid, Slider
from bokeh.models.glyphs import Line
from bokeh.io import curdoc, output_notebook
from bokeh.plotting import figure, show
from bokeh.layouts import widgetbox, row
from bokeh.models.widgets import Slider, DateRangeSlider
from bokeh.io import curdoc

DATA_SPLIT_THRESHOLD = 61


def read_data(filename):
    """Returns data into a pandas dataframe"""
    return pd.read_csv(filename)


def clean_data(data):
    """Removes columns that only contain nans and converts date column into a datetime object"""
    # Drop nas
    data = data.dropna(axis=1)

    # Convert date column into datetime
    data.date = pd.to_datetime(data.date)

    return data


def train_test_split(data):
    """Splits data into train and test.

        Train: First 2 months
        Test: Last month

        X: All columns apart from label
        y: Label column
        """
    y = data.label
    dates = data.date
    X = data.drop(['label', 'date'], axis=1)

    # Find date 2 months from initial date
    first_date = dates.min()
    two_month_date = first_date + timedelta(days=DATA_SPLIT_THRESHOLD)

    X_train = X[dates < two_month_date]
    y_train = y[dates < two_month_date]

    X_test = X[dates >= two_month_date]
    y_test = y[dates >= two_month_date]

    date_train = dates[dates < two_month_date]
    date_test = dates[dates >= two_month_date]

    return X_train, X_test, y_train, y_test, date_train, date_test


def train_model(model, X_train, y_train):
    """returns fitted model"""
    model.fit(X_train, y_train)
    return model


def predict_probs(model, X_test):
    """returns probabilities of classification from test set"""
    probas = model.predict_proba(X_test)
    return probas


def get_metric(probas, threshold, y_test):
    """returns classification metric value given a threshold, probas and true values"""
    preds = probas[:, 1] >= threshold
    TP = ((y_test[preds] == True) * 1).sum()
    FP = ((y_test[preds] == False) * 1).sum()
    metric = TP / (6 * FP + TP)
    return metric


def get_threshold(probas, y_test):
    """returns the best classification metric for different classfier"""
    thresholds = [i / 100 for i in range(100)]
    metrics = []
    final_metric = 0
    for threshold in thresholds:
        metric = get_metric(probas, threshold, y_test)
        if metric > final_metric:
            final_metric = metric
        metrics.append(final_metric)
    return thresholds[np.argmax(metrics)]


TP = lambda y_pred, y_true: np.logical_and(y_pred == 1, y_true == 1).sum()
FP = lambda y_pred, y_true: np.logical_and(y_pred == 1, y_true == 0).sum()
TN = lambda y_pred, y_true: np.logical_and(y_pred == 0, y_true == 0).sum()
FN = lambda y_pred, y_true: np.logical_and(y_pred == 0, y_true == 1).sum()


def recall(tp, fn):
    return tp / (tp + fn)


def precision(tp, fp):
    return tp / (tp + fp)


def metric_6(tp, fp):
    return tp / 6 * tp + fp


def main():
    # Step 0 - Prepare data
    data = read_data('data_itc.csv')
    data = clean_data(data)
    X_train, X_test, y_train, y_test, date_train, date_test = train_test_split(data)

    xg_class = xgb.XGBClassifier()

    forest = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)

    models = [xg_class, forest]
    names = ['XGBoost', 'Random Forest']
    model_probabilities, best_thresholds = [], []
    for model, name in zip(models, names):
        print(name)
        model = train_model(model, X_train, y_train)
        proba = predict_probs(model, X_test)
        model_probabilities.append(proba[:, 1])
        threshold = get_threshold(proba, y_test)
        best_thresholds.append(threshold)
        print('best', threshold)

    pred_df = pd.DataFrame({'proba_1_xg': model_probabilities[0],
                            'proba_1_rf': model_probabilities[1],
                            'date': date_test,
                            'y_true': y_test
                            })

    thresholds = pd.DataFrame({'thresholds': best_thresholds}, index=['xg', 'rf'])

    pred_df['pred_xgb'] = pred_df.proba_1_xg.values > thresholds.loc['xg', :].values
    pred_df['pred_rf'] = pred_df.proba_1_rf.values > thresholds.loc['rf', :].values

    dates = pred_df.date.unique()
    metrics = {date: {} for date in dates}

    for date in dates:
        print('\n', date)
        y_pred_day_xgb = pred_df[pred_df.date == date].pred_xgb
        y_pred_day_rf = pred_df[pred_df.date == date].pred_rf
        y_true_day = pred_df[pred_df.date == date].y_true

        recall_day_xgb = recall(TP(y_pred_day_xgb, y_true_day), FN(y_pred_day_xgb, y_true_day))
        precision_day_xgb = precision(TP(y_pred_day_xgb, y_true_day), FP(y_pred_day_xgb, y_true_day))
        print("XGBoost : Recall", recall_day_xgb, "Precision", precision_day_xgb)

        recall_day_rf = recall(TP(y_pred_day_rf, y_true_day), FN(y_pred_day_rf, y_true_day))
        precision_day_rf = precision(TP(y_pred_day_rf, y_true_day), FP(y_pred_day_rf, y_true_day))
        print("Random Forest : Recall", recall_day_rf, "Precision", precision_day_rf)

        metrics[date]['recall_xgb'] = recall_day_xgb
        metrics[date]['precision_xgb'] = precision_day_xgb
        metrics[date]['recall_rf'] = recall_day_rf
        metrics[date]['precision_rf'] = precision_day_rf

    # put everything into a DF
    metrics_df = pd.DataFrame.from_dict(metrics, orient='index')
    metrics_df['samples_number'] = pred_df.groupby('date').date.count().values
    metrics_df.head()

    # plot in server

    source = ColumnDataSource(data=dict(x=metrics_df.index, y=metrics_df.recall_xgb, y_=metrics_df.recall_rf))
    p = figure(plot_width=900, plot_height=300, x_axis_type='datetime', y_range=(0, 1))

    p.line(x='x', y='y', line_color="#d53e4f", line_width=2, line_alpha=0.6, source=source, legend='recall XGb')
    p.line(x='x', y='y_', line_color="#99d594", line_width=2, line_alpha=0.6, source=source, legend='recall Rf')

    slider = DateRangeSlider(start=metrics_df.index[0], end=metrics_df.index[-1],
                             value=(metrics_df.index[0], metrics_df.index[-1]), title="Date")

    def update(attr, old, new):
        start = slider.value[0]
        end = slider.value[1]

        new_source = ColumnDataSource(data=metrics_df.loc[start:end])
        print('jonas')
        source.data = new_source.data

    slider.on_change('value', update)
    show(row(slider, p))


if __name__ = '__main__':
    main()
