"""
Test dashboard for ITC

By Ariel Kibudi
"""

from bokeh.layouts import column, row
from bokeh.plotting import figure
from bokeh.io import curdoc
from collections import defaultdict
from bokeh.palettes import Category10_10
from bokeh.models.widgets import Dropdown
from bokeh.models import HoverTool
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Button, Paragraph
import os

import json
from datetime import datetime


class Dashboard(object):
    def __init__(self):
        self.results_path = os.path.join(os.path.dirname(__file__), 'results.json')
        self.dates_path = os.path.join(os.path.dirname(__file__), 'dates.json')
        self.roc_path = os.path.join(os.path.dirname(__file__), 'rocs.json')

    def run(self):
        """
        The main method to run the server
        :return:
        """
        output = Paragraph()

        f1 = self.get_f1s()

        with open(self.dates_path, 'r') as f:
            dates = json.load(f)

        model_menu = [("Random Forest", 'RF'), None, ("Logistic regression", 'LR'), None, ("SVM", 'SVM')]
        model_dropdown = Dropdown(label="Model", button_type="warning", menu=model_menu, value='RF')

        date_menu = self.make_date_menu(dates)
        date_dropdown = Dropdown(label="Date", button_type='primary', menu=date_menu, value='13/11/2017')

        output.text = model_dropdown.value + ' on the ' + date_dropdown.value

        def update(attr, old, new):
            model = model_dropdown.value
            d = date_dropdown.value

            output.text = model + ' on the ' + d
            source.data = self.get_roc_data(model, dates.index(d))

        model_dropdown.on_change('value', update)
        date_dropdown.on_change('value', update)

        roc = figure()

        source = ColumnDataSource(data=self.get_roc_data('RF', 5))

        roc.line('x', 'y', source=source)

        layout = row(f1, column(row(model_dropdown, date_dropdown), output, roc))

        curdoc().add_root(layout)

    def get_data(self):
        """
        Open the json with the data
        :return: Void
        """
        with open(self.results_path, 'r') as f:
            res = json.load(f)

        with open(self.dates_path, 'r') as f:
            dates = json.load(f)

        dates = [datetime.strptime(date, '%d/%m/%Y') for date in dates]

        data = defaultdict(list)

        for i, (k, v) in enumerate(res.items()):
            data['time'].append(dates)
            data['models'].append(k)
            data['f1'].append(v)
            data['color'].append(Category10_10[i])

        return data

    def get_f1s(self):
        """
        Get the f1 scores figure
        :return: The figure
        """
        source = ColumnDataSource(data=self.get_data())

        p = figure(x_axis_type="datetime", title="F1 scores")

        p.xaxis[0].formatter.days = '%d/%m/%Y'

        p.multi_line(xs='time', ys='f1', legend="models",
                     line_width=5, line_color='color', line_alpha=0.6,
                     hover_line_color='color', hover_line_alpha=1.0,
                     source=source)

        p.add_tools(HoverTool(show_arrow=False, line_policy='next', tooltips=[
            ('Model', '@models'),
            ('F1', '$data_y')]))

        p.legend.location = "center_left"
        p.legend.orientation = "vertical"

        return p

    def get_roc_data(self, model, d):
        """
        Get the Roc data from the json
        :param model: The model you want the roc for
        :param d: The date
        :return: (Dict) The ROC curve
        """
        with open(self.roc_path, 'r') as f:
            rocs = json.load(f)
        return {'x': rocs[model][d][0], 'y': rocs[model][d][1]}

    @staticmethod
    def make_date_menu(dates):
        """
        Makes the dates list
        :param dates:
        :return: (List) The dates
        """
        date_list = []
        for i, date in enumerate(dates):
            date_list.append((date, date))
        return date_list


def test():
    """
    Just to test
    :return: Void
    """
    output = Paragraph()

    button = Button(label="Say HI")

    layout = column(output, button)

    curdoc().add_root(layout)


def main():
    dashboard = Dashboard()
    dashboard.run()


# You must directly call the function! no if...
main()
# test()
